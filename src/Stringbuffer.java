package javac;

public class Stringbuffer {

	public static void main(String[] args) {
	
		//string is immutable
		//string is literal
		String s1="my";
		System.out.println(s1.hashCode());
		System.out.println(s1);
		
		s1=s1+" name";
		System.out.println(s1.hashCode());
		System.out.println(s1);
		
		s1=s1+" is";
		System.out.println(s1.hashCode());
		System.out.println(s1);
		
		s1=s1+" pravin";
		System.out.println(s1.hashCode());
		System.out.println(s1);
	
	
		
		
		//stringbuffer is mutable
		
		StringBuffer a=new StringBuffer("i");
		System.out.println(a);
		System.out.println(a.hashCode());
		
		StringBuffer b=new StringBuffer(" like icecream");
		
		a.append(b);
		System.out.println(a);
		System.out.println(a.hashCode());

		
		//string builder also mutable
		

	}

}
