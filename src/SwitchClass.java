package controlPackage;

public class SwitchClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int day = 3;
        System.out.println("-------Java Switch Case statement-------");
        switch(day){
        case 1:
        System.out.println("Today is Monday.");
        break;
        case 2:
        System.out.println("Today is Tuesday.");
        break;
        case 3:
        System.out.println("Today is Wednesday.");
        break;
        case 4:
        System.out.println("Today is Thursday."); 
        break;
        case 5:
        System.out.println("Today is Friday.");
        break;
	}
	}
}
