package javac;

public class StringSearching {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s1="Ramkumar";
		System.out.println(s1.indexOf('a'));//first occurence of 'a'
		System.out.println(s1.lastIndexOf('a'));//from last occerence it counts
		System.out.println(s1.lastIndexOf('s'));//if not present then give -ve result
		
		//-------------------------------------------------
		//String extraction methods
		
		System.out.println(s1.charAt(5));
		//System.out.println(s1.charAt(80));>>string index outof bound exception
		//System.out.println(s1.charAt(-1));
		System.out.println(s1.substring(3, 6));
	
	}
}
