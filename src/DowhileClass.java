package controlPackage;

public class DowhileClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

        int ctr =1;
        int maxCtr =5;
        System.out.println("----------Java do while loop-----------");
        do{      
        System.out.println("Hello World! Value Is : "+ctr);  
        ctr = ctr+1;
        }while(ctr<=maxCtr);
        System.out.println("-----------while loop------------");
        int a =1;
        int b =5;
        while(a<=b){      
            System.out.println(a);  
            a = a+1;
	    }

}
}
