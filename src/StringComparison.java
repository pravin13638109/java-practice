package javac;

public class StringComparison {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s1="Raaj";
		String s2="raaj";
		System.out.println(s1.equalsIgnoreCase(s2));
		s2="muthuRaj";
		System.out.println(s1.compareTo(s2));//compare based on lexicography result will +ve,o,-ve
		System.out.println(s1.compareToIgnoreCase(s2));
		System.out.println(s1.startsWith("r"));
		System.out.println(s2.startsWith("Raj", 05));
		s2="Raaj";
		System.out.println(s1.contentEquals(s2));//compare string or stringbuffer
		
	}

}
