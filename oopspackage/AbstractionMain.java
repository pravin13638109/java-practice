package oopspackage;

public class AbstractionMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Animal mybird=new Bird();
		Animal myfish=new Fish();
		System.out.println("-------My Bird details-------");
		mybird.label();
		mybird.eat();
		mybird.move();
		System.out.println("-------My fish details-------");
		myfish.label();
		myfish.eat();
		myfish.move();
	}

}
