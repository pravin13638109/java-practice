package functionalProgram;
//functional interface...>>contain only one abstract method
//annotation---->>Functional Interface

@FunctionalInterface
public interface InterfaceDemo {
    
	public abstract int add(int a,int b);
	
	default void sub() {
		System.out.println("i am default method");
	}
	static void multi(){
		System.out.println("i am static method");}

}
