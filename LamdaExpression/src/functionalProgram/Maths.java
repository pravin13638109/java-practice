package functionalProgram;

public class Maths implements InterfaceDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Dynamic binding--->use interface name itself for creating object
		
		InterfaceDemo m =new Maths();
		int c=m.add(4,6);
		System.out.println(c);
		m.sub();
		InterfaceDemo.multi();
	}

	@Override
	public int add(int a, int b) {
		// TODO Auto-generated method stub
		return a+b;
	}

	

}
