package polymorphismPackage;

public class BirdMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Bird myBird =  new Bird();
		myBird.fly();
		myBird.fly(10000);
		myBird.fly("eagle", 10000);
	}

}
