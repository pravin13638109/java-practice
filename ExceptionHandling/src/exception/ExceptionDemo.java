package exception;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 ExceptionDemo ea =new ExceptionDemo();
		   //ea.calculate();
		   //ea.getDetails();
		     ea.display();
	}
	
	public void display() {
		int n1=1,n2=0, n4=-5;
		try {
			System.out.println(n1/n2);
			int[] p = new int[n4];
		}
		catch(ArithmeticException|NegativeArraySizeException e) 
		{
			System.out.println(e.getMessage());
			System.out.println("Check");
		}}
	
	
	
	
	
	
	
	
	
	public void getDetails()  {
		Scanner s = new Scanner(System.in);
		
		
		
		try{
			System.out.println("Enter Account no:");
		    int accNo =s.nextInt();
		
		    System.out.println("Enter Pin no:");
	        int pinNo =s.nextInt();
	        System.out.println("Enter no of Employee");
	        int no =s.nextInt();
	       
	        int[] empDetails = new int[no];
			for(int i=1;i<no;i++) 
			{
				empDetails[i]=10000;
			}
			
		}
		
	    catch(InputMismatchException in) 
		{
			in.printStackTrace();
			System.out.println( in.getMessage());
	    	System.out.println("Check your AccNo/PinNo");
		}
		catch(ArrayIndexOutOfBoundsException ae) {
			
			System.out.println("Check array length");
		}
		catch(Exception ep)
		{
			System.out.println("Incorrect");
		}
		finally {
			System.out.println("Close the Transcation");
		}
		
		
	}
	
	
	
    public void calculate() throws ArithmeticException,InputMismatchException,NegativeArraySizeException {
		   Scanner n = new Scanner(System.in);
		   System.out.println("Enter the no:");
		   int a=n.nextInt();
		   int b=n.nextInt();
		
		
		System.out.println(a+b);
		System.out.println(a-b);
		System.out.println(a*b);
		
		try {
		System.out.println(a/b);
		}
		
		catch (Exception e) {
			System.out.println("Check your divisor");
			calculate();
		}
		

}}
