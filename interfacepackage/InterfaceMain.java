package interfacepackage;

public class InterfaceMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Eagle myEagle =  new Eagle();
		System.out.println("------Eagle details------");
		myEagle.eat();
		myEagle.sound();
		myEagle.fly();

		System.out.println("Number of legs: "  + Bird.numberOfLegs);
		System.out.println("Outer covering: "  + Bird.outerCovering);
		}	
	}
